import data from '../data/data.json' assert { type: 'json' }
const {biography: {fullname, paragraphs}} = data;

function renderBiography() {
    let biographyElem = document.querySelector('.biography');
    let fullnameElem = document.createElement("p");
    fullnameElem.classList.add("biography__fullname")
    fullnameElem.innerText = fullname
    biographyElem.appendChild(fullnameElem)
    
    paragraphs.forEach(text => {
        let p = document.createElement("p")
        p.innerText = text
        biographyElem.appendChild(p)
    })
}

renderBiography()