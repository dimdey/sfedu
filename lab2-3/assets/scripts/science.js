import data from '../data/data.json' assert { type: 'json' }
const { science } = data;

function renderScience() {
    let container = document.querySelector('.science-works')

    science.forEach(science => {
        let item = document.createElement('div')
        item.classList.add('science__item')
        item.innerHTML = science
        container.appendChild(item)
    })
}

renderScience()