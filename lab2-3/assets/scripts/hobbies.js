import data from '../data/data.json' assert { type: 'json' }
const { hobbies } = data;

function renderHobbies() {
    let container = document.querySelector('.hobby-container')

    hobbies.forEach(hobby => {
        let item = document.createElement('div')
        item.classList.add('hobby__item')
    
        let image = document.createElement('div')
        image.classList.add('left__image')
        image.innerHTML = `<img src=${hobby.image}>`

        let text = document.createElement('div')
        text.classList.add('right__text')
        text.innerText = hobby.text

        item.appendChild(image)
        item.appendChild(text)
        container.appendChild(item)
    })
}

renderHobbies()