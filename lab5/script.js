const root = document.querySelector("#root")
//.replace(/[0-9]/gim,'<span class="red">$1</span>')
const getData = async ()=>{
    const request = await fetch("https://www.woman.ru/news/poslednie-novosti-o-sostoyanii-zdorovya-nikolaeva-v-reanimaciyu-ne-puskayut-zhenu-id909650/", {
        headers: {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
        }
    })
    const siteHtml = await request.text()
    root.innerHTML = siteHtml

    root.textContent = root.textContent.replace("[-]{0,1}[\d]*[.]{0,1}[\d]+", function(x) {
        return `<span class="red">${x}</span>`
    })
}

getData()