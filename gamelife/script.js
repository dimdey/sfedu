var canvas = document.getElementById("game");
var ctx = canvas.getContext('2d');
var stateButton = document.querySelector(".controls .state");
var resetButton = document.querySelector(".controls .reset");
var generationText = document.querySelector(".info__generation")
var aliveText = document.querySelector(".info__alive")

var time = 0;
var isStarted = false;
var timerId;

var cells = []

var windowWidth = 500;
var windowHeight = 500;
var cellSize = 10;
var cellWidth = windowWidth / cellSize;
var cellHeight = windowHeight / cellSize; 

function updateGame() {
    if (isStarted) {
        clearInterval(timerId);
        stateButton.innerHTML = "НАЧАТЬ";
    }
    else {
        timerId = setInterval(iterateTime, 100)
        stateButton.innerHTML = "СТОП";
    }
    isStarted = !isStarted;
    render();
}

function resetCells(update) {
    time = 0;
    for(let i = 0; i < cellWidth; i++) {
        cells[i] = []
        for(let j = 0; j < cellHeight; j++) {
            cells[i][j] = 0
        }
    }
    render();
    if (update) {
        updateGame()
    }
}

function iterateTime() {
    time++;
    for(let x = 0; x < cellWidth; x++) {
        for(let y = 0; y < cellHeight; y++) {
            var neighboursCount = checkNeighbours(x, y);
            if(cells[x][y] == 0 && neighboursCount == 3) {
                cells[x][y] = 1;
                continue;
            }

            if(cells[x][y] == 1 && (neighboursCount > 3 || neighboursCount < 2)) {
                cells[x][y] = 0;
                continue;
            }
        }
    }

    render();
}

function checkNeighbours(x, y) {
    let neighbours = 0;
    for(let offsetX = -1; offsetX <= 1; offsetX++) {
        for(let offsetY = -1; offsetY <= 1; offsetY++) {
            let neighX = x + offsetX;
            let neighY = y + offsetY;

            if ((neighX < 0 || neighX > cellWidth - 1) 
                || 
                (neighY < 0 || neighY > cellHeight - 1)) 
            {
                continue;
            }
            
            if(cells[neighX][neighY] == 1) {
                neighbours++;
            }
        }
    }
    return neighbours;
}

/*
    DRAWING
*/

function render() {
    let alive = 0;
    ctx.clearRect(0, 0, windowWidth, windowHeight)
    for(let i = 0; i < cellWidth; i++) {
        drawLine(i*cellSize, 0, i*cellSize, windowHeight)
        for(let j = 0; j < cellHeight; j++) {
            drawLine(0, j*cellSize, windowWidth, j*cellSize)
            if(cells[i][j] == 1) {
                alive++;
                ctx.fillRect(i*cellSize, j*cellSize, cellSize, cellSize);
            }
        }
    }

    generationText.innerHTML = time;
    aliveText.innerHTML = alive;
}


function drawLine(startX, startY, endX, endY) {
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(endX, endY);
    ctx.stroke();
}

canvas.onclick = function(event) {
    var x = Math.floor(event.offsetX / cellSize);
    var y = Math.floor(event.offsetY / cellSize);
    cells[x][y] = !cells[x][y];
    render();
}

stateButton.onclick = updateGame;
resetButton.onclick = () => resetCells(isStarted);

resetCells(false);

